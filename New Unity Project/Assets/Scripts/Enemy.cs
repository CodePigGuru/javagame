﻿using UnityEngine;
using System.Collections;

[RequireComponent (typeof (NavMeshAgent ))]

public class Enemy : LivingEntity {
	NavMeshAgent pathfinder;
	Transform target;
	// Use this for initialization
	protected override void Start () {
		base.Start();
		pathfinder = GetComponent<NavMeshAgent> ();
		target = GameObject.FindGameObjectWithTag ("Player").transform;
	}
	
	// Update is called once per frame
	void Update () {
		StartCoroutine (updatePath ());
	}

	IEnumerator updatePath() { 
		float refreshRate = 0.25f;

		while (target != null) {
			Vector3 targetPosistion = new Vector3(target.position.x, 0, target.position.z);
			if (!dead){
			pathfinder.SetDestination (targetPosistion);
			}
			yield return new WaitForSeconds(refreshRate);
		}
	}
}
