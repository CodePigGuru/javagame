﻿using UnityEngine;
using System.Collections;

public class LivingEntity : MonoBehaviour,IDamageable {

	public float startingHealth;
	protected float health;
	protected bool dead;

	public event System.Action onDeath;
	protected virtual void Start(){
		health = startingHealth;
	}
	public void takeHit(float damage, RaycastHit hit){
		health -= damage;

		if (health <= 0 && !dead) {
			die();
		}
	}

	protected void die(){
		dead = true;
		if (onDeath != null) {
			onDeath ();
		}
		GameObject.Destroy (gameObject);
	}
}
