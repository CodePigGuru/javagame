using UnityEngine;
using System.Collections;

public class Spawner : MonoBehaviour {
	public Wave[] waves;
	public Enemy enemy;

	Wave currentWave;
	int currentWaveNumber;
	int enemiesRemainingAlive;
	int enemiesRemainingToSpawn;
	float nextSpawnTime;

	void Start(){
		nextWave ();
	}

	void Update() {
		if (enemiesRemainingToSpawn > 0 && Time.time > nextSpawnTime) {
			enemiesRemainingToSpawn--;
			nextSpawnTime = Time.time + currentWave.timeBetweenSpawns;

			Enemy spawnedEnemy = Instantiate(enemy, Vector3.zero, Quaternion.identity) as Enemy;
			spawnedEnemy.onDeath += OnEnemyDeath;
		}
	}

	void OnEnemyDeath(){
		enemiesRemainingAlive--;

		if (enemiesRemainingAlive == 0) {
			nextWave ();
		}

	}

	void nextWave(){
		currentWaveNumber++;
		if (currentWaveNumber - 1 < waves.Length) {
			currentWave = waves [currentWaveNumber - 1];

			enemiesRemainingToSpawn = currentWave.enemyCount;
			enemiesRemainingAlive = enemiesRemainingToSpawn;
		}
		}
	[System.Serializable]
	public class Wave {
		public int enemyCount;
		public float timeBetweenSpawns;
}
}
